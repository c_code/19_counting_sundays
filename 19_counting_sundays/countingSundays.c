/* countingSundays
 *
 *If Jan 1 1990 was a monday this program finds the number of times the first of the month is on a sunday from jan 1 1901 to dec 31 2000
 *
 *Author: Jeremy Tobac
 *Date: October 2014
 */
#include <stdio.h>
#include <stdlib.h>

int main()
{
	int intDays, intYears, intMonths, intFirsts;
	int aintMonths[4] = {30, 31, 28, 29};
	intFirsts = intDays = intYears = intMonths = 0;


	for(intYears = 1900; intYears < 2001; intYears++){
		for(intMonths = 0; intMonths < 12; intMonths++){
			if(intMonths == 0 || intMonths == 2 || intMonths == 4 || intMonths == 6 || intMonths == 7 || intMonths == 9 || intMonths == 11){
				intDays+=aintMonths[1];/*31 days*/
			}
			else if(intMonths == 3 || intMonths == 5 || intMonths == 8 || intMonths == 10){
				intDays+=aintMonths[0];/*30 days*/
			}
			else if((intYears % 4) == 0 && ((intYears % 100) != 0 || (intYears % 400) == 0)){/*div by 4 not div by 100 unless div by 400*/
				intDays+=aintMonths[3];/*leap year*/
			}
			else{
				intDays+=aintMonths[2];/*28 days*/
			}
			if((intDays % 7) == 6){
				if(intYears > 1900){
					intFirsts++;
				}
			}
			else if((intDays % 7) == 0){
				intDays = 0;
			}
		}
	}

	printf("Number of first of the months that fell on a Sunday: %d\n", intFirsts);

	return 0;
}
